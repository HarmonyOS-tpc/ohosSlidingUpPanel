/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sothree.slidinguppanel.sample.adapter;

import com.sothree.slidinguppanel.sample.ResourceTable;
import com.sothree.slidinguppanel.sample.slice.MainAbilitySlice;
import ohos.agp.components.*;

import java.util.List;

/**
 * SampleListItemProvider.
 */
public class SampleListItemProvider extends BaseItemProvider {
    private MainAbilitySlice mainAbilitySlice;
    private List<String> listdetails;

    /**
     * Constructor
     *
     * @param mainAbilitySlice MainAbilitySlice
     * @param mlistdetails List<String>
     */
    public SampleListItemProvider(MainAbilitySlice mainAbilitySlice, List<String> mlistdetails) {
        this.mainAbilitySlice = mainAbilitySlice;
        listdetails = mlistdetails;
    }

    @Override
    public int getCount() {
        return listdetails.size();
    }

    @Override
    public Object getItem(int position) {
        return listdetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        return getRootView(position);
    }

    private Component getRootView(int position) {
        Component rootView = LayoutScatter.getInstance(mainAbilitySlice)
                .parse(ResourceTable.Layout_sample_list, null, false);
        if (rootView.findComponentById(ResourceTable.Id_text_details) instanceof Text) {
            Text textNameName = (Text) rootView.findComponentById(ResourceTable.Id_text_details);
            textNameName.setText(listdetails.get(position));
        }
        return rootView;
    }
}

