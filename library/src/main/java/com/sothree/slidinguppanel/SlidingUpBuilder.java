/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sothree.slidinguppanel;

import ohos.agp.animation.Animator;
import ohos.agp.components.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * SlidingUpBuilder.
 */
public final class SlidingUpBuilder {
    SlidingUpPanel.State mStartState = SlidingUpPanel.State.HIDDEN;
    List<SlidingUpPanel.Listener> mListeners = new ArrayList<>();
    int mStartGravity = Gravity.BOTTOM;
    boolean mDebug = false;
    int mAutoSlideDuration = Constants.FIVE_HUNDERED_NUM_CONSTANT;
    boolean isGesturesEnabled = true;
    int isInterpolator = Animator.CurveType.DECELERATE;
    Component mAlsoScrollView;
    float mDensity;
    float mTouchableArea;
    boolean isRtl;
    Component mSliderView;
    private boolean isStateRestored = false;

    /**
     * * Constructor
     *
     * @param sliderView Component
     */
    public SlidingUpBuilder(Component sliderView) {
        if (sliderView != null) {
            mSliderView = sliderView;
            mDensity = sliderView.getResourceManager().getDeviceCapability()
                    .screenDensity / Constants.HUNDERED_SIXTY_NUM_CONST;
        }
        isRtl = false;
    }

    /**
     * * withSlideFromOtherView
     *
     * @param startState int
     * @return SlidingUpBuilder
     */
    public SlidingUpBuilder withStartState(SlidingUpPanel.State startState) {
        if (!isStateRestored) {
            mStartState = startState;
        }
        return this;
    }

    /**
     * * withSlideFromOtherView
     *
     * @param gravity int
     * @return SlidingUpBuilder
     */
    public SlidingUpBuilder withStartGravity(int gravity) {
        if (!isStateRestored) {
            mStartGravity = gravity;
        }
        return this;
    }

    /**
     * * withListeners
     *
     * @param listeners View
     * @return SlidingUpBuilder
     */
    public SlidingUpBuilder withListeners(List<SlidingUpPanel.Listener> listeners) {
        if (listeners != null) {
            mListeners.addAll(listeners);
        }
        return this;
    }

    /**
     * * withListeners
     *
     * @param listeners View
     * @return SlidingUpBuilder
     */
    public SlidingUpBuilder withListeners(SlidingUpPanel.Listener listeners) {
        List<SlidingUpPanel.Listener> listenerArrayList = new ArrayList<>();
        Collections.addAll(listenerArrayList, listeners);
        return withListeners(listenerArrayList);
    }

    /**
     * * withLoggingEnabled
     *
     * @param isEnabled boolean
     * @return SlidingUpBuilder
     */
    public SlidingUpBuilder withLoggingEnabled(boolean isEnabled) {
        if (!isStateRestored) {
            mDebug = isEnabled;
        }
        return this;
    }

    /**
     * * withGesturesEnabled
     *
     * @param isEnabled View
     * @return SlidingUpBuilder
     */
    public SlidingUpBuilder withGesturesEnabled(boolean isEnabled) {
        isGesturesEnabled = isEnabled;
        return this;
    }

    /**
     * * withSlideFromOtherView
     *
     * @param alsoScrollView View
     * @return SlidingUpBuilder
     */
    public SlidingUpBuilder withSlideFromOtherView(Component alsoScrollView) {
        mAlsoScrollView = alsoScrollView;
        return this;
    }

    /**
     * * build
     *
     * @return SlidingUpBuilder
     */
    public SlidingUpPanel build() {
        return new SlidingUpPanel(this);
    }
}