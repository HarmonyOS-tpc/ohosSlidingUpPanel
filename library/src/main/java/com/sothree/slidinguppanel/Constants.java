/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sothree.slidinguppanel;

/**
 * Function description
 * class Constants
 */
public class Constants {
    /**
     * Invalid ID
     */
    public static final int INVALID = -1;

    /**
     * TEN_NUM_CONST ID
     */

    public static final int TEN_NUM_CONST = 10;

    /**
     * FIVE_HUNDERED_NUM_CONSTANT ID
     */
    public static final int FIVE_HUNDERED_NUM_CONSTANT = 500;

    /**
     * MAGIC_160 ID
     */
    public static final float HUNDERED_SIXTY_NUM_CONST = 160f;

    /**
     * HUNDERED_NUM_CONST ID
     */
    public static final short HUNDERED_NUM_CONST = 100;

    /**
     * FIVE_NUM_CONST ID
     */
    public static final float FIVE_NUM_CONST = 5f;

    /**
     * TWO_HUNDERED_NUM_CONST ID
     */
    public static final int TWO_HUNDERED_NUM_CONST = 200;

    /**
     * ONE_NUM_CONSTANT ID
     */
    public static final int ONE_NUM_CONSTANT = 1;

    /**
     * THIRTY_NUM_CONST ID
     */
    public static final long THIRTY_NUM_CONST = 30;

    /**
     * EIGHTY_NUM_CONST ID
     */
    public static final float EIGHTY_NUM_CONST = 80;

    /**
     * ZERO_NUM_CONST ID
     */
    public static int ZERO_NUM_CONST = 0;

    private Constants() {
    }
}
