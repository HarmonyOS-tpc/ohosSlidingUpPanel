/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sothree.slidinguppanel;

import ohos.agp.components.Component;
import ohos.multimodalinput.event.TouchEvent;

/**
 * SlidingUpVerticalTouchEvent.
 */
class SlidingUpVerticalTouchEvent extends SlidingTouchEvent {
    private boolean isGoingUp = false;
    private boolean isGoingDown = false;

    SlidingUpVerticalTouchEvent(SlidingUpBuilder builder, NotifierDetails notifier,
                                AnimationProcessor animationProcessor) {
        super(builder, notifier, animationProcessor);
    }

    boolean consumeBottomToTop(Component touchedView, TouchEvent event) {
        float touchedArea = event.getPointerPosition(0).getY();
        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                mViewHeight = mBuilder.mSliderView.getHeight();
                mStartPositionY = event.getPointerScreenPosition(0).getY();
                mViewStartPositionY = mBuilder.mSliderView.getTranslationY();
                isCanSlide = touchFromAlsoSlide(touchedView, event);
                isCanSlide |= mBuilder.mTouchableArea >= touchedArea;
                break;
            case TouchEvent.POINT_MOVE:
                float difference = event.getPointerScreenPosition(0).getY() - mStartPositionY;
                float moveTo = mViewStartPositionY + difference;
                float percents = moveTo * Constants.HUNDERED_NUM_CONST / mBuilder.mSliderView.getHeight();
                calculateDirection(event);

                if (moveTo > 0 && isCanSlide) {
                    mNotifier.notifyPercentChanged(percents);
                    mBuilder.mSliderView.setTranslationY(moveTo);
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                float slideAnimationFrom = mBuilder.mSliderView.getTranslationY();
                if (slideAnimationFrom == mViewStartPositionY) {
                    return !CheckPoint.isUpEventInView(mBuilder.mSliderView, event);
                }
                boolean isScrollableAreaConsumed = mBuilder.mSliderView.getTranslationY()
                        > mBuilder.mSliderView.getHeight() / Constants.FIVE_NUM_CONST;

                if (isScrollableAreaConsumed && isGoingDown) {
                    mAnimationProcessor.setValuesAndStart(slideAnimationFrom,
                            mBuilder.mSliderView.getHeight() - Constants.TWO_HUNDERED_NUM_CONST);
                } else {
                    mAnimationProcessor.setValuesAndStart(slideAnimationFrom, 0);
                }
                isCanSlide = true;
                isGoingUp = false;
                isGoingDown = false;
                break;
            default:
                throw new IllegalArgumentException("You are using not supported value");
        }
        mPrevPositionY = event.getPointerScreenPosition(0).getY();
        mPrevPositionX = event.getPointerScreenPosition(0).getX();
        return true;
    }

    boolean consumeTopToBottom(Component touchedView, TouchEvent event) {
        float touchedArea = event.getPointerPosition(0).getY();
        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                mViewHeight = mBuilder.mSliderView.getHeight();
                mStartPositionY = event.getPointerScreenPosition(0).getY();
                mViewStartPositionY = mBuilder.mSliderView.getTranslationY();
                isCanSlide = touchFromAlsoSlide(touchedView, event);
                isCanSlide |= getBottom() - mBuilder.mTouchableArea <= touchedArea;
                break;
            case TouchEvent.POINT_MOVE:
                float difference = event.getPointerScreenPosition(0).getY() - mStartPositionY;
                float moveTo = mViewStartPositionY + difference;
                float percents = moveTo * Constants.HUNDERED_NUM_CONST / -mBuilder.mSliderView.getHeight();
                calculateDirection(event);

                if (moveTo < 0 && isCanSlide) {
                    mNotifier.notifyPercentChanged(percents);
                    mBuilder.mSliderView.setTranslationY(moveTo);
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                float slideAnimationFrom = -mBuilder.mSliderView.getTranslationY();
                if (slideAnimationFrom == mViewStartPositionY) {
                    return !CheckPoint.isUpEventInView(mBuilder.mSliderView, event);
                }
                boolean isScrollableAreaConsumed = mBuilder.mSliderView.getTranslationY()
                        < -mBuilder.mSliderView.getHeight() / Constants.FIVE_NUM_CONST;

                if (isScrollableAreaConsumed && isGoingUp) {
                    mAnimationProcessor.setValuesAndStart(-slideAnimationFrom,
                            mBuilder.mSliderView.getHeight() + mBuilder.mSliderView.getTop());
                } else {
                    mAnimationProcessor.setValuesAndStart(-slideAnimationFrom, 0);
                }
                isCanSlide = true;
                break;
            default:
                throw new IllegalArgumentException("You are using not supported value");
        }
        mPrevPositionY = event.getPointerScreenPosition(0).getY();
        mPrevPositionX = event.getPointerScreenPosition(0).getX();
        return true;
    }

    private void calculateDirection(TouchEvent event) {
        isGoingUp = mPrevPositionY - event.getPointerScreenPosition(0).getY() > 0;
        isGoingDown = mPrevPositionY - event.getPointerScreenPosition(0).getY() < 0;
    }
}
