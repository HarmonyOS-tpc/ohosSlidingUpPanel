/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sothree.slidinguppanel;

import ohos.agp.components.Component;
import ohos.multimodalinput.event.TouchEvent;

/**
 * SlidingTouchEvent.
 */
class SlidingTouchEvent {
    SlidingUpBuilder mBuilder;
    AnimationProcessor mAnimationProcessor;
    boolean isCanSlide = true;
    NotifierDetails mNotifier;

    float mViewHeight;

    float mStartPositionY;
    volatile float mPrevPositionY;
    volatile float mPrevPositionX;
    float mViewStartPositionY;

    SlidingTouchEvent(SlidingUpBuilder builder, NotifierDetails notifier,
                    AnimationProcessor animationProcessor) {
        mBuilder = builder;
        mAnimationProcessor = animationProcessor;
        mNotifier = notifier;
    }

    int getTop() {
        return mBuilder.mSliderView.getTop();
    }

    int getBottom() {
        return mBuilder.mSliderView.getBottom();
    }

    boolean touchFromAlsoSlide(Component touchedView, TouchEvent event) {
        return touchedView == mBuilder.mAlsoScrollView;
    }
}
