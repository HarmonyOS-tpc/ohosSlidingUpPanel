# ohosSlidingUpPanel
  ohosSlidingUpPanel: This library provides a simple way to add a touchable sliding up panel.
 
# ohosSlidingUpPanel includes:
* SlidingUpPanel is moving the layout panel top to bottom and reverse.
* it includes the hide and show panel based on the click event and popup list text component.
* it includes panel ListContainer to scrolling the list data
* implemented touchevent for moving the layout panel top and bottom.
* ohosSlidingUpPanel used to move the layout top to bottom and bottom to top with the help of touch event.

# Usage Instructions
1. A sample project which provides runnable code examples that demonstrate uses of the classes in this project is available in the sample/ folder.

2. The following core classes are the essential interface to SlidingUpPanel:
SlidingUpBuilder: The entry point for using slidingUp panel holds the  slidingUpPanel and manages the TouchEvent animation scrolling features.
AnimationProcessor: Manages the top and bottom animation slidingup panel.

3. The steps to initialize the SlidingUpPanel and the core SlidingUpBuilder classes:
	      slidingUpPanel = new SlidingUpBuilder(sliderView)
                .withListeners(new SlidingUpPanel.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {
                        dim.setAlpha(1 - (percent / Constants.HUNDERED_NUM_CONST));
                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                    }
                })
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                .withStartState(SlidingUpPanel.State.HIDDEN)
                .withSlideFromOtherView(findComponentById(ResourceTable.Id_rootView))
                .build();


# Installation instruction

1.For using ohosSlidingUpPanel module in sample app,include the below library dependency to generate hap/library.har:
  Add the dependencies in entry/build.gradle as below :

        dependencies {
                implementation project(path: ':library')
        }

2. Using the library.har, make sure to add library.har file in the entry/libs folder and add the below dependency
   in build.gradle.
   Modify the dependencies in the entry/build.gradle file.

          dependencies {
              implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
          }

3. For using ohosSlidingUpPanel from a remote repository in separate application, add the below dependencies and include "library.har" in libs folder of "entry" module  :
    Modify entry build.gradle as below :
    	
    	dependencies {
    	    implementation fileTree(dir: 'libs', include: ['*.har'])
    	    implementation 'io.openharmony.tpc.thirdlib:ohosSlidingUpPanel:1.0.1'
    }
	
# License
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this work except in compliance with the License.
  You may obtain a copy of the License in the LICENSE file, or at:

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and limitations
 under the License.

